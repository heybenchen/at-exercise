import React, { Component } from 'react';
import moment from 'moment';

class TimelineRuler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      earliestTime: moment(this.props.earliestTimelineItem.start),
      latestTime: moment(this.props.latestTimelineItem.end),
    }
  }

  daysInRuler() {
    return this.state.latestTime.diff(this.state.earliestTime, 'days') + 1;
  }

  renderRuler() {
    const style = {
      width: `${this.props.zoomUnit}px`,
    };

    return new Array(this.daysInRuler()).fill(0).map((element, index) => (
      <div key={index} className='timeline-ruler-item' style={style}>
        {this.state.earliestTime.clone().add(index, 'days').format('MMM DD')}
      </div>
    ));
  }

  render() {
    const timelineRuler = this.renderRuler();

    return (
      <div className='timeline-ruler-container'>
        {timelineRuler}
      </div>
    );
  }
}

export default TimelineRuler;
