import React, { Component } from 'react';
import moment from 'moment';

class TimelineItem extends Component {
  daysInRow() {
    const {start, end} = this.props.timelineItem;
    return moment(end).diff(moment(start), 'days') + 1;
  }

  render() {
    const timelineItemWidth = Math.max(this.daysInRow() * this.props.zoomUnit, this.props.zoomUnit);
    const timelineItemTitle = `${this.props.timelineItem.name}: ${this.props.timelineItem.start} - ${this.props.timelineItem.end}`;
    const style = {
      width: `${timelineItemWidth}px`,
    }

    return (
      <div className='timeline-item-wrapper'>
        <div
          className='timeline-item'
          title={timelineItemTitle}
          style={style}
        >
          {this.props.timelineItem.name}
        </div>
      </div>
    );
  }
}

export default TimelineItem;
