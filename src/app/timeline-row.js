import React, { Component } from 'react';
import TimelineItem from './timeline-item';
import moment from 'moment';

class TimelineRow extends Component {
  daysBetweenItems(time1, time2) {
    const moment1 = moment(time1);
    const moment2 = moment(time2);
    return moment2.diff(moment1, 'days');
  }

  renderTimelineItems() {
    return this.props.timelineItems.map((timelineItem, index, array) => {
      let emptyDaysCount;
      let emptyDays;
      if (index === 0) {
        emptyDaysCount = this.daysBetweenItems(this.props.timelineStart, timelineItem.start);
        emptyDays = this.renderEmptyDays(emptyDaysCount);
      } else if (index > 0) {
        emptyDaysCount = this.daysBetweenItems(array[index - 1].end, timelineItem.start) - 1;
        emptyDays = this.renderEmptyDays(emptyDaysCount);
      }

      return (
        <React.Fragment key={index}>
          {emptyDays}
          <TimelineItem
            key={timelineItem.id}
            timelineItem={timelineItem}
            zoomUnit={this.props.zoomUnit}
          />
        </React.Fragment>
      )
    });
  }

  renderEmptyDays(days) {
    const style = {
      width: days * this.props.zoomUnit
    };
    return (
      <div className='timeline-empty-days' style={style}></div>
    );
  }

  render() {
    const timelineItems = this.renderTimelineItems();

    return (
      <div className='timeline-row'>
        {timelineItems}
      </div>
    );
  }
}

export default TimelineRow;
