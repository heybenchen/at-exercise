export function sortByStartDate(timelineItems) {
  return timelineItems.slice().sort((a, b) => {
    return new Date(a.start) - new Date(b.start);
  });
}

export function findEarliestItem(timelineItems) {
  return timelineItems.reduce((a, b) => {
    return a.start < b.start ? a : b;
  });
}

export function findLatestItem(timelineItems) {
  return timelineItems.reduce((a, b) => {
    return a.end > b.end ? a : b;
  });
}
