import React, { Component } from 'react';
import { sortByStartDate, findEarliestItem, findLatestItem } from './utilities';
import TimelineRow from './timeline-row';
import TimelineRuler from './timeline-ruler';

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = { timelineRows: [] }
  }

  placeInTimelineRow(newTimelineItem) {
    const newTimelineItemStartTime = new Date(newTimelineItem.start);
    const existingRow = this.state.timelineRows.find(timelineRow => {
      const latestTimelineItem = findLatestItem(timelineRow);
      return new Date(latestTimelineItem.end) < newTimelineItemStartTime;
    });
    if (!existingRow) {
      return this.state.timelineRows.push([newTimelineItem]);
    }
    return existingRow.push(newTimelineItem);
  }

  renderTimelineRuler() {
    return (
      <TimelineRuler
        earliestTimelineItem={findEarliestItem(this.props.timelineItems)}
        latestTimelineItem={findLatestItem(this.props.timelineItems)}
        zoomUnit={this.props.zoomUnit}
      />
    );
  }

  renderTimelineRows() {
    const timelineItems = sortByStartDate(this.props.timelineItems);
    const timelineStart = findEarliestItem(this.props.timelineItems).start;
    timelineItems.forEach(timelineItem => this.placeInTimelineRow(timelineItem));
    return this.state.timelineRows.map((timelineRow, index) => (
      <TimelineRow
        key={index}
        timelineItems={timelineRow}
        timelineStart={timelineStart}
        zoomUnit={this.props.zoomUnit}
      />
    ));
  }

  componentWillReceiveProps() {
    this.setState({ timelineRows: [] });
  }

  render() {
    const timelineRuler = this.renderTimelineRuler();
    const timelineRows = this.renderTimelineRows();

    return (
      <div>
        {timelineRuler}
        {timelineRows}
      </div>
    );
  }
}

export default Timeline;
