import React from 'react';
import { render } from 'react-dom';
import timelineItems from './timelineItems';
import Timeline from './app/timeline';
import './index.css'

const App = () => (
  <div>
    <Timeline timelineItems={timelineItems} zoomUnit={30}/>
  </div>
);

render(<App />, document.getElementById('root'));
