# Prompt
## High level objective:

Design and implement a component for visualizing events on a timeline.

## Details:

Your timeline layout should arrange events in a compact space-efficient way: generally speaking, if event A ends before event B starts, the bars for those events can share the same horizontal lane, instead of existing on separate lanes. You may want to slightly relax this constraint to fit in the name of the event (for example, if the event's bar is too short, or the event's name is too long).

The input to the component should be an array of events, where each event has a name, start date, and end date.

The start and end dates will be formatted as YYYY-MM-DD date strings, for example: “2018-12-23”. You don't need to worry about hours, minutes, seconds, or time zones.

You can assume every event's end date is the same or later than its start date.

Avoid using libraries that solve too much of the problem. General purpose libraries like React are definitely okay, but a library that calculates the layout for a timeline is not, for example. This also applies to the CSS Grid `grid-auto-flow` property (but you may use CSS Grid for positioning).

After you have a basic read-only timeline showing up, here are some potential improvements to attempt:

* Allow zooming in and out of the timeline.
* Allow dragging and dropping to change the start date and/or end date for an event.
* Allow editing the name of events inline.
* Any other polish or useful enhancements you can think of.

Include a README that covers:

* How long you spent on the assignment.
* What you like about your implementation.
* What you would change if you were going to do it again.
* How you made your design decisions. For example, if you looked at other timelines for inspiration, please note that.
* How you would test this if you had more time.

If you did not use the starter code, please also include instructions on how to build and run your project so we can see and interact with the timeline component you built. It should render the sample data included in "src/timelineItems.js"

What we're looking for:

* Clean, readable, maintainable code.
* A sensible user experience and design for the final product.

## Starter code:

To use the starter code: navigate to this project directory, run `npm install` to install dependencies (this takes a couple minutes), and then run `npm start` to initialize and connect to a node server with your default browser. Please feel free to use as much or as little of the starter code as you'd like.

## Sample data:

The "src/timelineItems.js" file has some sample data you can use to get started.

# Prompt response
![Screenshot](https://content.evernote.com/shard/s172/sh/e2fd8636-04cf-4d0a-95d5-3a501081057a/7c160018ac4d2def/res/c95fb259-5434-4a8d-b178-c5eb06ca81ad/skitch.png)

## Time spent
This exercise took me roughly 5 hours to fulfill the basic requirements to my satisfaction.
Some caveats:
* I have not been actively coding for the last 12-18 months.
* I took some time to refresh my familiarity with React.
* I would not ship these changes without writing tests, clarifying requirements with a PM/Designer, and walking through my approach with another engineer first.
* I deliberately submitted this prompt without making everything perfect to meet the original time constaints. If this was a real project, I'd pause here to get feedback.

## Implementation
Some principles:
* Start simple. Avoid introducing complexity or support for potential features until necessary.
* Get it working first, optimize and clean up later.
* Minimize coupling. Use components and props to maintain a sane architecture.
* Make it look visually decent.

![Architecture](https://content.evernote.com/shard/s172/sh/a0cdf10f-3e5e-4983-99bc-32d7b590f8f5/865ead376f0118cd/res/98060ebe-375b-4c2d-b744-96bb9ac2b9b3/skitch.png)
* **Timeline**: The main parent component responsible for taking the timeline data and rendering the TimelineRuler and TimelineRows.
* **TimelineRuler**: A strip at the top of the timeline that breaks the UI into vertical columns of "days".
* **TimelineRow**: A single row in the timeline. Each row is optimized to use as much horizontal space as possible without causing overlap in TimelineItems.
* **TimelineItem** A single timeline event.

#### What I like about the implementation
Components are broken up into sane sub-components. Code is fairly straightforward and hopefully easy to reason about. With the exception of `zoomUnit`, I didn't add much more than necesssary. Visually looks alright (simple but easy to understand) and would be easy to tweak.

#### What you would change if you were going to do it again
I wouldn't necessarily change my approach if I were to do it again. If I had more time, I would refine this approach and prioritize the features that I would build next. Some things I would consider:
* Creating data models for timeline items and timeline rows. Right now I'm using basic javascript objects to read the timeline data, but having dedicated models for them would allow me to work with them more easily. For example, I'm often converting the `start` and `end` strings into date objects. Having a model would allow me to create a getter that does that for me. A property like `startDate` and `endDate` for an entire TimelineRow would be neat. A wrapper that lets me get the first and last item in a list of TimelineItems would be cleaner than having a utility method.
* Decide what I want to do with `zoomUnit`. I was originally thinking about having a mouse zoom or slider to adjust the zoom of the timeline, but I don't think that would actually be especially useful. What could be neat is being able to set the zoom of the timeline as "days/weeks/months/years", which would allow customers to get a longer term view of their timeline. I'd have to adjust how I render TimelineRuler to support this.
* Display the whole event on hover. Right now if you hover over a timeline item that you can't fully read, it shows the full name and date. I'd like to make it so that on hover the entire object grows or a nicer tooltip shows up.

#### How you made your design decisions
My visual design is heavily influence by Asana's timeline, which I regularly use for projects on my team. The technical design is my own creation based on what seemed necessary (and no more).

#### How you would test this if you had more time
With more time, I'd prioritize making unit tests to verify the utility and helper methods in every class. There's some computational code that is finicky (e.g., occasionally adding `1` to a number, doing sorts, grabbing first/last elements. I'd want to make sure those are tested thoroughly as they're most likely to get mixed up.
After that, I would consider adding functional tess to verify behavior. As of right now, there's not much in terms of interactivity that I would prioritize.
In the longer run, some kind of visual regression test could also be useful, but I would hold off unless this was a shared project that was frequently being changed by multiple people.
